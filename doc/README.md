# Capteur de mouvement avec un ESP8266

## TL;DR

Cet article vise à créer un petit capteur de mouvement. Par exemple, détecter si quelqu'un rentre chez soi, ou combien de fois mon chat vient me voir chaque nuit.

On vise donc quelques objectifs : 

 - [ ] Un capteur simple à mettre en place,
 - [ ] Fonctionnant en Wifi,
 - [ ] Avec un historique de données,
 - [ ] Pouvant m'alerter avec des notifications Push (sur mobile),
 - [ ] Et capable d'être connecté à d'autres solutions IOT; dans notre cas la [Green Solution](https://greensystemes.com/nos-offres/green-solution).

## Le matériel

Rien de plus simple :

 - Un NodeMCU ESP8266,
 - Un capteur SR602,
 - Des fils,
 - Une batterie portable.
